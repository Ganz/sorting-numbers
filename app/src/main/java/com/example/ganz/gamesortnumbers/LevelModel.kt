package com.example.ganz.gamesortnumbers

import android.view.animation.Animation

data class LevelModel(
        var randomNumbers: String,
        var buttonColor: Int,
        var animation: Animation
)