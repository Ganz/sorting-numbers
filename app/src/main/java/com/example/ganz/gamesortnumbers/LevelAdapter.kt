package com.example.ganz.gamesortnumbers

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ganz.minigamesortnumbers.R
import kotlinx.android.synthetic.main.cardview_number.view.*


class LevelAdapter(
        private val data: ArrayList<LevelModel>,
        private val onClickListener: (position: Int) -> Unit):
        RecyclerView.Adapter<LevelAdapter.SquareNumberViewHolder>() {

    override fun onBindViewHolder(holder: SquareNumberViewHolder, position: Int) {
        holder.btnNumbers.text = data[position].randomNumbers
        holder.btnNumbers.setBackgroundResource(data[position].buttonColor)
        holder.btnNumbers.startAnimation(data[position].animation)
        holder.btnNumbers.setOnClickListener { onClickListener(position) }
    }

    override fun getItemCount(): Int = data.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SquareNumberViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view: View = inflater.inflate(R.layout.cardview_number, parent, false)
        return SquareNumberViewHolder(view)
    }

    fun clearItem() {
        notifyDataSetChanged()
    }

    fun clearAndAddItem(position: Int, add: Int) {
        data[position].randomNumbers = add.toString()
        data[position].buttonColor = R.drawable.ic_tilenumber
        //data[position].animation = anim
        notifyDataSetChanged()
    }

    class SquareNumberViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val btnNumbers = itemView.btn_numbers!!
    }

}