package com.example.ganz.gamesortnumbers

import com.example.ganz.gamesortnumbers.Constants.PREF_HIGH_SCORE_GO_HIGH
import com.orhanobut.hawk.Hawk

object HawkUtils {

    var scoreGoHigh: String
        get() = Hawk.get(PREF_HIGH_SCORE_GO_HIGH, "")
        set(value) {
            Hawk.put(PREF_HIGH_SCORE_GO_HIGH, value)
        }
}