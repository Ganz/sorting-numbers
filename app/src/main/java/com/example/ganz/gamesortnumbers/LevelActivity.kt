package com.example.ganz.gamesortnumbers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.animation.AnimationUtils
import android.widget.Button
import com.example.ganz.minigamesortnumbers.R
import kotlinx.android.synthetic.main.level.*
import java.util.*
import kotlin.collections.ArrayList


class LevelActivity : AppCompatActivity() {

    private var numbersList: ArrayList<LevelModel>? = null
    private var numbersAdapter: LevelAdapter? = null
    private var counter = 1
    private var infinityCounter = 65
    private var pStatus = 100
    private val handler = Handler()
    private val arrHint = arrayOfNulls<Int>(64)
    private val arrPosition = arrayOfNulls<Int>(64)
    private var j = 1
    private var k = 1
    private var score = "0"

    /*private val animFadeIn = AnimationUtils.loadAnimation(
            baseContext,
            R.anim.anim_fade_in)!!
*/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.level)
        score = HawkUtils.scoreGoHigh

        setSupportActionBar(tbr_level)
        if (HawkUtils.scoreGoHigh != "") {
            tv_current_record.text = score
        }
        shuffleNumbers(64.toString())
        //shuffleLetters(64.toString())
        initRecycler()
        initProgressBar()
        btn_back.setOnClickListener { finish() }
        /*val animFadeIn = AnimationUtils.loadAnimation(
                baseContext,
                R.anim.anim_fade_in)!!*/
        btn_hint.setOnClickListener { hint() }
        "Go!!".toast(this)
    }

    private fun setScore() {
        HawkUtils.scoreGoHigh = counter.toString()
    }

    override fun onBackPressed() {
        showDialogTimeIsUp()
    }

    private fun initProgressBar() {
        progressBar.progress = 100
        progressBar.max = 100
        progressBar.secondaryProgress = 100

        Thread(Runnable {
            while (pStatus>0) {
                pStatus -= 1

                handler.post {
                    progressBar.progress = pStatus
                }
                try {
                    Thread.sleep(100)
                }
                catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                when {
                    pStatus <= 0 -> this.runOnUiThread {
                        if (!this.isFinishing) { showDialogTimeIsUp() }
                        }
                }
            }
        }).start()
    }

    private fun showDialogTimeIsUp() {
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle("Time is up")
        alertDialog.setMessage("Try again")

        val modeLayout = this.inflate(R.layout.alert_try_again)
        alertDialog.setView(modeLayout)

        modeLayout.findViewById<Button>(R.id.btn_try_again).setOnClickListener {
            val intent = Intent(this, LevelActivity::class.java)
            intent.putExtra("key", 1)
            finish()
            startActivity(intent)
        }
        modeLayout.findViewById<Button>(R.id.btn_end_game).setOnClickListener { finish() }
        alertDialog.show()
    }

    private fun shuffleNumbers(rangeOfNumbers: String) {
        val arrTest = arrayOfNulls<Int>(100)
        for (i in arrTest.indices) {
            arrTest[i] = i
        }
        Arrays.asList(arrTest).shuffle()

        numbersList = ArrayList()

        for (i in 1 until rangeOfNumbers.toInt()+1) {
            numbersList!!.add(arrTest[i]?.let { LevelModel(it.toString(), R.drawable.ic_tilenumber,
                    AnimationUtils.loadAnimation(
                            baseContext,
                            R.anim.anim_none)!!) }!!)
        }
        numbersList!!.shuffle()
    }

    private fun shuffleLetters(rangeOfNumbers: String) {
        val arrLetters = arrayOfNulls<Char>(100)
        for (i in arrLetters.indices) {
            arrLetters[i] = (32+i).toChar()
        }
        Arrays.asList(arrLetters).shuffle()
        numbersList = ArrayList()


        for (i in 1 until rangeOfNumbers.toInt()+1) {
            numbersList!!.add(arrLetters[i]?.let { LevelModel(it.toString(), R.color.white,
                    AnimationUtils.loadAnimation(
                            baseContext,
                            R.anim.anim_none)!!) }!!)
        }
        numbersList!!.shuffle()

    }
//not ready yet
    private fun shuffleArithmetics(rangeOfNumbers: String) {
        val arrDefault = arrayOfNulls<Int>(100)
        for (i in arrDefault.indices) {
            arrDefault[i] = i
        }
        Arrays.asList(arrDefault).shuffle()

//initializing datum

        val arrTable = arrayOf<Array<String>>()
        // j - type of arithmetic
        // i - range of indexes
        for (i in 0..(rangeOfNumbers.toInt()+1)) {
            for (j in 0..(rangeOfNumbers.toInt()+1)) {
                arrTable[1][j] = 1.toString()
            }
        }

//printing data
        for (i in 0..(rangeOfNumbers.toInt()+1)) {
            for (j in 0..(rangeOfNumbers.toInt()+1)) {
                println(arrTable[i][j])
            }
        }

        numbersList = ArrayList()

        /*val arrTest = arrayOfNulls<Int>(100)
        for (i in arrTest.indices) {
            arrTest[i] = i
        }
        Arrays.asList(arrTest).shuffle()

        numbersList = ArrayList()

        for (i in 1 until rangeOfNumbers.toInt()+1) {
            numbersList!!.add(arrTest[i]?.let { LevelModel(it.toString(), R.drawable.ic_tilenumber,
                    AnimationUtils.loadAnimation(
                            baseContext,
                            R.anim.anim_none)!!) }!!)
        }
        numbersList!!.shuffle()*/
    }

    private fun initRecycler() {
        val layoutManager = GridLayoutManager(this, 8)
        this.rv_level.layoutManager = layoutManager
        numbersAdapter = LevelAdapter(numbersList!!) { this.onItemClick(it) }
        this.rv_level.adapter = numbersAdapter
    }

    private fun onItemClick(position: Int) {
        when {
            numbersList!![position].randomNumbers == "" -> "Try again".toast(this)
            numbersList!![position].randomNumbers.toInt() == counter -> {
                //numbersAdapter!!.clearItem(position)
                /*if ((counter) == score.toInt()) {
                }*/
                setScore()
                counter++

                /*numbersList!![position].animation =  AnimationUtils.loadAnimation(
                        this, R.anim.anim_fade_in)
                numbersList!![arrPosition[k-1]!!].animation =  AnimationUtils.loadAnimation(
                        this, R.anim.anim_none)
                arrPosition[k] = position
                k++*/
                arrHint[0] = 0
                numbersList!![arrHint[j-1]!!].animation =  AnimationUtils.loadAnimation(
                        this, R.anim.anim_none)
                numbersAdapter!!.clearAndAddItem(position, infinityCounter)
                infinityCounter++
                pStatus = 100
                tv_nextNumber.text = counter.toString()
                //Showing score progress
                /*if ((counter-1) == score.toInt()) {
                    tv_new_record.text = (counter-1).toString() + " / "
                    tv_current_record.text = (counter-1).toString()
                    score = (counter).toString()
                }
                else
                    tv_new_record.text = (counter-1).toString() + " / "*/

            }
            else -> "Try again".toast(this)
        }
    }

    private fun hint() {
        arrHint[0] = 0
        for (i in 1 until 64) {
            when {
                numbersList!![i].randomNumbers == counter.toString() -> {
                    numbersList!![i].buttonColor = R.drawable.ic_tilenumber
                    numbersList!![i].animation =  AnimationUtils.loadAnimation(
                            this, R.anim.anim_fade_in)
                    numbersAdapter!!.clearItem()
                    numbersList!![arrHint[j-1]!!].animation =  AnimationUtils.loadAnimation(
                            this, R.anim.anim_none)
                    arrHint[j] = i
                    j++
                }
            }
        }
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocalHelper.onAttach(newBase))
    }
}