package com.example.ganz.gamesortnumbers

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.TextView
import com.example.ganz.minigamesortnumbers.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    var context = null

    //private val sharedPreferences = this.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_start_game.setOnClickListener { showDialogModes() }
        btn_ranks.setOnClickListener { showDialogRanks() }
        btn_settings.setOnClickListener { showDialogSettings() }
        btn_quit_game.setOnClickListener { showDialogQuit() }
    }

    /*private fun getScore() {
        val score = sharedPreferences.getString(HIGH_SCORE_GO_HIGH, "")
    }*/

    private fun showDialogRanks() {
        val alertDialog = AlertDialog.Builder(
                this, R.style.AlertDialogCustom)
        val ranksLayout = this.inflate(R.layout.alert_ranks)
        alertDialog.setView(ranksLayout)
        val dialog = alertDialog.create()
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)

        val score = HawkUtils.scoreGoHigh
        if (HawkUtils.scoreGoHigh != "") {
            ranksLayout.findViewById<TextView>(R.id.tv_score).text = score
        }

        dialog.show()
    }

    private fun showDialogQuit() {
        val alertDialog = AlertDialog.Builder(
                this, R.style.AlertDialogCustom)
        val quitLayout = this.inflate(R.layout.alert_quit)
        alertDialog.setView(quitLayout)
        alertDialog.setPositiveButton("YES"){ _, _ ->
            finish()
        }

        alertDialog.setNegativeButton("No"){ _, _ ->
        }

        val dialog = alertDialog.create()
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()
    }

    private fun showDialogSettings() {
        val alertDialog = AlertDialog.Builder(
                this, R.style.AlertDialogCustom)
        val settingsLayout = this.inflate(R.layout.alert_settings)
        alertDialog.setView(settingsLayout)
        settingsLayout.findViewById<Button>(R.id.btn_left).setOnClickListener {
            val resources = LocalHelper.setLocale(this, "ru").resources
            settingsLayout.findViewById<TextView>(R.id.tv_settings).text =
                    resources.getString(R.string.settings)
            settingsLayout.findViewById<Button>(R.id.btn_sound).text =
                    resources.getString(R.string.sound)
            settingsLayout.findViewById<Button>(R.id.btn_languages).text =
                    resources.getString(R.string.languages)
            btn_start_game.text = resources.getString(R.string.start)
            btn_ranks.text = resources.getString(R.string.ranks)
            btn_settings.text = resources.getString(R.string.settings)
            btn_quit_game.text = resources.getString(R.string.quit)
        }
        settingsLayout.findViewById<Button>(R.id.btn_right).setOnClickListener {
            val resources = LocalHelper.setLocale(this, "en").resources
            settingsLayout.findViewById<TextView>(R.id.tv_settings).text =
                    resources.getString(R.string.settings)
            settingsLayout.findViewById<Button>(R.id.btn_sound).text =
                    resources.getString(R.string.sound)
            settingsLayout.findViewById<Button>(R.id.btn_languages).text =
                    resources.getString(R.string.languages)
            btn_start_game.text = resources.getString(R.string.start)
            btn_ranks.text = resources.getString(R.string.ranks)
            btn_settings.text = resources.getString(R.string.settings)
            btn_quit_game.text = resources.getString(R.string.quit)
        }

        alertDialog.setPositiveButton("YES"){ _, _ ->
            "Saved".toast(this)
        }

        val dialog = alertDialog.create()
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()
    }

    private fun startGame() {
        val intent = Intent(this, LevelActivity::class.java)
        intent.putExtra("key", 1)
        startActivity(intent)
    }

  /*  private fun runAnimationVova() {
        val anim = AnimationUtils.loadAnimation(
                this,
                R.anim.test)
        btn_mode_1.startAnimation(anim)
    }*/

    private fun showDialogModes() {
        val alertDialog = AlertDialog.Builder(
                this, R.style.AlertDialogCustom)
        val modeLayout = this.inflate(R.layout.alert_mode)
        alertDialog.setView(modeLayout)

        modeLayout.findViewById<Button>(R.id.btn_mode_1).setOnClickListener { startGame() }

        val dialog = alertDialog.create()
        dialog.window.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.show()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocalHelper.onAttach(newBase))
    }
}
