package com.example.ganz.gamesortnumbers

object Constants {
    const val PREF_HIGH_SCORE_GO_HIGH = "pref_high_score_go_high"
    const val DEFAULT_LANGUAGE = "ru"
}