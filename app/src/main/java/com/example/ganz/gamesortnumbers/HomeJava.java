package com.example.ganz.gamesortnumbers;

import android.app.Application;
import android.content.Context;

import com.example.ganz.gamesortnumbers.LocalHelper;

public class HomeJava extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocalHelper.onAttach(base, "en"));
    }
}
