package com.example.ganz.minigamesortnumbers

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_start_game.setOnClickListener { startGame(8) }
        btn_quit_game.setOnClickListener { finish() }
    }

    private fun startGame(rowsColumns: Int) {
        val intent = Intent(this, LevelActivity::class.java)
        intent.putExtra("key", 1)
        startActivity(intent)
    }
}
