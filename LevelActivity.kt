package com.example.ganz.minigamesortnumbers

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import kotlinx.android.synthetic.main.level.*
import java.util.*
import kotlin.collections.ArrayList


class LevelActivity : AppCompatActivity() {

    private var numbersList: ArrayList<LevelModel>? = null
    private var numbersAdapter: LevelAdapter? = null
    private var counter = 1
    private var pStatus = 100
    private val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.level)

        setSupportActionBar(tbr_level)
        shuffleNumbers(64.toString())
        initRecycler()
        initProgressBar()
        btn_back.setOnClickListener { finish() }

        Toast.makeText(this, "Heeeeey!!", Toast.LENGTH_SHORT).show()

    }

    private fun initProgressBar() {
        progressBar.progress = 100
        progressBar.max = 100
        progressBar.secondaryProgress = 100

        Thread(Runnable {
            while (pStatus>0) {
                pStatus -= 1

                handler.post {
                    progressBar.progress = pStatus
                }
                try {
                    Thread.sleep(1000)
                }
                catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
            Toast.makeText(this, "Time is up!!", Toast.LENGTH_SHORT).show()
        }).start()
    }

    private fun shuffleNumbers(rangeOfNumbers: String) {
        val arrTest = arrayOfNulls<Int>(1000)
        for (i in arrTest.indices) {
            arrTest[i] = i
        }
        Arrays.asList(arrTest).shuffle()

        numbersList = ArrayList()

        for (i in 1 until rangeOfNumbers.toInt()+1) {
            numbersList!!.add(arrTest[i]?.let { LevelModel(it.toString(), R.color.unClicked) }!!)
        }
        numbersList!!.shuffle()
    }

    private fun initRecycler() {
        val layoutManager = GridLayoutManager(this, 8)
        this.rv_level.layoutManager = layoutManager as RecyclerView.LayoutManager?
        numbersAdapter = LevelAdapter(numbersList!!) { this.onItemClick(it) }
        this.rv_level.adapter = numbersAdapter
    }

    private fun onItemClick(position: Int) {
        if (numbersList!![position].randomNumbers.toInt() == counter) {
            numbersAdapter!!.clearItem(position)
            counter++
            tv_nextNumber.text = counter.toString()
        }
        else {
            Toast.makeText(
                    this,
                    "Try again",
                    Toast.LENGTH_SHORT).show()
        }
    }
}