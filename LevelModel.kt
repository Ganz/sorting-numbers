package com.example.ganz.minigamesortnumbers

data class LevelModel(
        var randomNumbers: String,
        var buttonColor: Int
)