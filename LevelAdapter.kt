package com.example.ganz.minigamesortnumbers

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.cardview_number.view.*

class LevelAdapter(
        private val data: ArrayList<LevelModel>,
        private val onClickListener: (position: Int) -> Unit):
        RecyclerView.Adapter<LevelAdapter.SquareNumberViewHolder>() {

    override fun onBindViewHolder(holder: SquareNumberViewHolder, position: Int) {
        holder.btnNumbers.text = data[position].randomNumbers
        holder.btnNumbers.setBackgroundResource(data[position].buttonColor)
        holder.btnNumbers.setOnClickListener { onClickListener(position) }
    }

    override fun getItemCount(): Int = data.count()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SquareNumberViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view: View = inflater.inflate(R.layout.cardview_number, parent, false)
        return SquareNumberViewHolder(view)
    }

    fun clearItem(position: Int) {
        data[position].randomNumbers = ""
        data[position].buttonColor = R.color.clicked
        notifyDataSetChanged()
    }

    class SquareNumberViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val btnNumbers = itemView.btn_numbers
    }

}